<?php

namespace Drupal\entity_generic\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm;

/**
 * Provides an entities deletion confirmation form.
 */
class GenericDeleteMultipleForm extends DeleteMultipleForm {

}
