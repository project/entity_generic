<?php

namespace Drupal\entity_generic\Access;

use Drupal\entity\EntityAccessControlHandler;

/**
 * Controls access based on the generic entity permissions.
 *
 * @see \Drupal\entity\UncacheableEntityPermissionProvider
 */
class GenericAccessControlHandler extends EntityAccessControlHandler {

}
