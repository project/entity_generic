<?php

namespace Drupal\entity_generic\Access;

/**
 * Defines the access control handler for the entity type.
 *
 * @see \Drupal\entity_generic\Entity\GenericType
 */
class GenericTypeAccessControlHandler extends GenericConfigAccessControlHandler {

}
