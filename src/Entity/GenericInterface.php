<?php

namespace Drupal\entity_generic\Entity;

/**
 * Defines an interface for generic entities.
 */
interface GenericInterface extends SimpleInterface, EntityTypedInterface {

}
