<?php

namespace Drupal\entity_generic\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a type entity.
 */
interface GenericConfigInterface extends ConfigEntityInterface {

}
